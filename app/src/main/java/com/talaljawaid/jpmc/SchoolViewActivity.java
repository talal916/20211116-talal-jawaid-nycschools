package com.talaljawaid.jpmc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

public class SchoolViewActivity extends AppCompatActivity implements OnMapReadyCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_view);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);


        ((TextView) findViewById(R.id.school_name)).setText(getIntent().getStringExtra("schoolName").replace("Â",""));
        ((TextView) findViewById(R.id.school_view_reading_score)).setText(getIntent().getStringExtra("satScoreReading"));
        ((TextView) findViewById(R.id.school_view_writing_score)).setText(getIntent().getStringExtra("satScoreWriting"));
        ((TextView) findViewById(R.id.school_view_math_score)).setText(getIntent().getStringExtra("satScoreMath"));
        ((TextView) findViewById(R.id.school_view_overview_body)).setText(getIntent().getStringExtra("overview").replace("Â",""));

    }

    // Get a handle to the GoogleMap object and display marker.
    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(Double.parseDouble(getIntent().getStringExtra("latitude")),
                Double.parseDouble(getIntent().getStringExtra("longitude")));
        float zoom = 16;
        googleMap.setBuildingsEnabled(false);
        googleMap.setIndoorEnabled(false);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }


}