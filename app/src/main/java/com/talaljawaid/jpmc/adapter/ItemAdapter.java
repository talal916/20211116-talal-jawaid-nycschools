package com.talaljawaid.jpmc.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.talaljawaid.jpmc.R;
import com.talaljawaid.jpmc.SchoolViewActivity;
import com.talaljawaid.jpmc.model.School;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {
    private List<School> dataset;
    private Context context;
    private School item;

    public ItemAdapter(Context context, List<School> dataset) {
        this.context = context;
        this.dataset = dataset;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View adapterLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ItemViewHolder(adapterLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        item = dataset.get(position);
        String totalScore = Integer.toString(item.getTotalSatScore());
        holder.schoolName.setText(item.getName());
        holder.satScoreMath.setText(item.getSatScore().getMathScore());
        holder.satScoreReading.setText(item.getSatScore().getReadingScore());
        holder.satScoreWriting.setText(item.getSatScore().getWritingScore());
        holder.satScoreTotal.setText(totalScore);
        holder.schoolAddress.setText(item.getAddress());
        holder.schoolPhone.setText(item.getPhoneNumber());
        holder.setSchool(item);

    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView schoolName;
        TextView schoolAddress;
        TextView schoolPhone;
        TextView satScoreReading;
        TextView satScoreWriting;
        TextView satScoreMath;
        TextView satScoreTotal;
        School school;

        public ItemViewHolder(View view) {
            super(view);
            this.schoolName = view.findViewById(R.id.school_name);
            this.schoolAddress = view.findViewById(R.id.school_address);
            this.schoolPhone = view.findViewById(R.id.school_phone);
            this.satScoreReading = view.findViewById(R.id.sat_score_reading);
            this.satScoreWriting = view.findViewById(R.id.sat_score_writing);
            this.satScoreMath = view.findViewById(R.id.sat_score_math);
            this.satScoreTotal = view.findViewById(R.id.sat_score_total);
            MaterialCardView materialCardView = view.findViewById(R.id.card_view);
            materialCardView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            System.out.println("Clicked!");
            context.startActivity(constructIntent(v));


        }

        private Intent constructIntent(View v) {
            Intent intent = new Intent(v.getContext(), SchoolViewActivity.class);
            intent.putExtra("schoolName", school.getName());
            intent.putExtra("schoolAddress", school.getAddress());
            intent.putExtra("schoolPhone", school.getPhoneNumber());
            intent.putExtra("satScoreReading", school.getSatScore().getReadingScore());
            intent.putExtra("satScoreWriting", school.getSatScore().getWritingScore());
            intent.putExtra("satScoreMath", school.getSatScore().getMathScore());
            intent.putExtra("satScoreTotal", school.getTotalSatScore());
            intent.putExtra("city", school.getCity());
            intent.putExtra("overview", school.getOverview());
            intent.putExtra("latitude", school.getLatitude());
            intent.putExtra("longitude", school.getLongitude());
            intent.putExtra("state_code", school.getState_code());
            intent.putExtra("zip", school.getZipCode());
            return intent;
        }

        public void setSchool(School item) {
            school = item;
        }
    }

}
