package com.talaljawaid.jpmc;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.talaljawaid.jpmc.adapter.ItemAdapter;
import com.talaljawaid.jpmc.model.RequestManager;
import com.talaljawaid.jpmc.model.SchoolDataManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    String filename_scores = "sat_scores_file6";
    String filename_schools = "nyc_schools_file6";

    // Whenever network call is made, response is saved to avoid future network calls.
    private void saveFile(String fileName, JSONArray fileContents) {
        try {
            String jsonArrayToString = fileContents.toString(2);
            File file = new File(getApplicationContext().getFilesDir(), fileName);
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(jsonArrayToString);
            oos.flush();
            oos.close();
        } catch (Exception e) {
            System.out.println("Exception while writing file.");
            e.printStackTrace();
        }
    }

    @Nullable
    private List<JSONObject> readFile(String fileName) {
        List<JSONObject> jsonObjectList = new ArrayList<>();
        try {
            File file = new File(getApplicationContext().getFilesDir(), fileName);
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            String savedResponse = (String) ois.readObject();
            JSONArray savedResponseJsonArray = new JSONArray(savedResponse);

            for (int i = 0; i < savedResponseJsonArray.length(); i++) {
                        JSONObject jsonObject = savedResponseJsonArray.getJSONObject(i);
                        jsonObjectList.add(jsonObject);
            }
            return jsonObjectList;
        } catch (Exception e) {
            System.out.println("Exception while reading file.");
            e.printStackTrace();
        }
        return null;
    }

    private boolean fileExists(String fileName) {
        System.out.println(getApplicationContext().getFilesDir());
        File file = new File(getApplicationContext().getFilesDir(), fileName);
        // Had issues with empty files being counted as existing
        if(file.exists() && file.length() > 100) return true;
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SchoolDataManager schoolDataManager = new SchoolDataManager();
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        final RequestManager requestManager = new RequestManager(getApplicationContext());


        // Load saved file and avoid network request if possible.
        // Reduces load time from ~5s to <0.5s.
        // TODO: Button that allows you to manually refresh instead of loading locally stored data.
        if(fileExists(filename_schools)) {
            List<JSONObject> savedResponse = readFile(filename_schools);
            if( savedResponse != null) {
                schoolDataManager.parseSchools(savedResponse);
            }
            // If scores and schools have been loaded, then set recyclerView.
            if(schoolDataManager.getScores() != null) {
                schoolDataManager.mergeScoresAndSchools();
                schoolDataManager.sortSchools();
                recyclerView.setAdapter(new ItemAdapter(MainActivity.this, schoolDataManager.getSchools()));
            }
        } else {
            requestManager.volleyGet(RequestManager.SCHOOLS_REQUEST_URL, new RequestManager.VolleyCallback() {
                @Override
                public void getResponse(JSONArray response) {
                    saveFile(filename_schools, response);
                    List<JSONObject> savedResponse = readFile(filename_schools);
                    if( savedResponse != null) {
                        schoolDataManager.parseSchools(savedResponse);
                    }
                    if (schoolDataManager.getScores() != null) {
                        schoolDataManager.mergeScoresAndSchools();
                        schoolDataManager.sortSchools();
                        recyclerView.setAdapter(new ItemAdapter(MainActivity.this, schoolDataManager.getSchools()));

                    }
                }
            });
        }

        if(fileExists(filename_scores)) {
            List<JSONObject> savedResponse = readFile(filename_scores);
            if( savedResponse != null) {
                schoolDataManager.parseScores(savedResponse);
            }
            // If scores and schools have been loaded, then set recyclerView.
            if(schoolDataManager.getSchools() != null) {
                schoolDataManager.mergeScoresAndSchools();
                schoolDataManager.sortSchools();
                recyclerView.setAdapter(new ItemAdapter(MainActivity.this, schoolDataManager.getSchools()));
            }
        } else {
            requestManager.volleyGet(RequestManager.SCORES_REQUEST_URL, new RequestManager.VolleyCallback() {
                @Override
                public void getResponse(JSONArray response) {
                    saveFile(filename_scores, response);
                    List<JSONObject> savedResponse = readFile(filename_scores);
                    if( savedResponse != null) {
                        schoolDataManager.parseScores(savedResponse);
                    }
                    if (schoolDataManager.getSchools() != null) {
                        schoolDataManager.mergeScoresAndSchools();
                        schoolDataManager.sortSchools();
                        recyclerView.setAdapter(new ItemAdapter(MainActivity.this, schoolDataManager.getSchools()));
                    }
                }
            });
        }
    }




}