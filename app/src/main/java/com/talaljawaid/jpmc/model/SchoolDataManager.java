package com.talaljawaid.jpmc.model;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class SchoolDataManager {
    List<School> schools;
    HashMap<String, SatScore> scores;

    public SchoolDataManager() {
    }

    // Only called once both scores and schools are populated
    // O(n) where n is number of schools.
    public void mergeScoresAndSchools() {
        List<School> newSchoolList = new ArrayList<>();

        for(School school : schools) {
            String dbn = school.getDbn();
            if(scores.containsKey(dbn)) {
                try {
                    Integer.parseInt(scores.get(dbn).getMathScore());
                    Integer.parseInt(scores.get(dbn).getReadingScore());
                    Integer.parseInt(scores.get(dbn).getWritingScore());
                    school.setSatScore(scores.get(dbn));
                    newSchoolList.add(school);
                } catch (Exception e) {
                    System.out.println("Invalid Scores! Not adding school dbn: " +school.getDbn());
                    e.printStackTrace();
                }

            }
        }
        schools = newSchoolList;
    }

    public void sortSchools() {
        Comparator<School> comparator = Comparator.comparing(School::getTotalSatScore).reversed();
        Collections.sort(schools, comparator);
    }


    public void parseSchools(List<JSONObject> response) {
        List<School> localSchools = new ArrayList<>();
            for (JSONObject jsonObject : response) {
                try {
                    String school_name = jsonObject.getString("school_name");
                    String dbn = jsonObject.getString("dbn");
                    String address = jsonObject.getString("primary_address_line_1");
                    String phone = jsonObject.getString("phone_number");
                    String zip = jsonObject.getString("zip");
                    String overview = jsonObject.getString("overview_paragraph");
                    String longitude = jsonObject.getString("longitude");
                    String latitude = jsonObject.getString("latitude");
                    String city = jsonObject.getString("city");
                    String state_code = jsonObject.getString("state_code");
                    localSchools.add(new School( dbn, school_name, address, phone, zip, new SatScore(dbn,"","",""), overview, longitude, latitude, city, state_code));
                } catch (JSONException e) {
                    System.out.println("Exception while parsing school");
                    e.printStackTrace();
                }
            }
            this.schools = localSchools;
            System.out.println("Schools.size(): " + schools.size());
    }

    public void parseScores(List<JSONObject> response) {
        HashMap<String, SatScore> scoreHashMap = new HashMap<>();
        try {
            for (JSONObject jsonObject : response) {
                String dbn = jsonObject.getString("dbn");
                String readingScore = jsonObject.getString("sat_critical_reading_avg_score");
                String mathScore = jsonObject.getString("sat_math_avg_score");
                String writingScore = jsonObject.getString("sat_writing_avg_score");
                scoreHashMap.put(dbn, new SatScore(dbn, readingScore, mathScore, writingScore));
            }
            this.scores = scoreHashMap;
            System.out.println("Scores.size(): " + scores.size());
        } catch (JSONException e) {
            System.out.println("Exception while parsing scores.");
            e.printStackTrace();
        }
    }

    @Nullable
    public HashMap<String, SatScore> getScores() {
        return scores;
    }

    @Nullable
    public List<School> getSchools() {
        return schools;
    }

}
