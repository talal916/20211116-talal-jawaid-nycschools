package com.talaljawaid.jpmc.model;

import java.util.Comparator;

public class School implements Comparator<School>{
    private String dbn;
    private String name;
    private String address;
    private String phoneNumber;
    private String zipCode;
    private SatScore satScore;
    private String overview;
    private String longitude;
    private String latitude;
    private String city;
    private String state_code;

    public School(String dbn, String name, String address, String phoneNumber, String zipCode,
                  SatScore satScore, String overview, String longitude, String latitude,
                  String city, String state_code)  {
        this.dbn = dbn;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.zipCode = zipCode;
        this.satScore = satScore;
        this.overview = overview;
        this.longitude = longitude;
        this.latitude = latitude;
        this.city = city;
        this.state_code = state_code;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getLongitude() { return longitude; }

    public String getLatitude() {
        return latitude;
    }

    public String getCity() {
        return city;
    }

    public String getState_code() {
        return state_code;
    }

    public String getDbn() { return dbn;}

    public SatScore getSatScore() {
        return satScore;
    }

    public int getTotalSatScore() {
        return Integer.parseInt(satScore.getMathScore()) + Integer.parseInt(satScore.getReadingScore()) + Integer.parseInt(satScore.getWritingScore());
    }

    public String getOverview() {
        return overview;
    }
    public void setSatScore(SatScore satScore) {
        this.satScore = satScore;
    }


    @Override
    public int compare(School o1, School o2) {
        return o2.getTotalSatScore() - o1.getTotalSatScore();
    }


}
