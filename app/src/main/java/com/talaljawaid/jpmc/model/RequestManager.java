package com.talaljawaid.jpmc.model;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class RequestManager {
    public static final String SCHOOLS_REQUEST_URL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json";
    public static final String SCORES_REQUEST_URL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json";
    private Context context;

    public RequestManager(Context context) {
        this.context = context;
    }

    public void volleyGet(String url, final VolleyCallback callback){

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        StringRequest jsonArrayRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    String fixedDecodingResponse = new String(response.getBytes(StandardCharsets.ISO_8859_1),"UTF-8");
                    JSONArray jsonArray = new JSONArray(response);
                    callback.getResponse(jsonArray);
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        requestQueue.add(jsonArrayRequest);

    }

    public interface VolleyCallback {
        void getResponse(JSONArray response);
    }



}




