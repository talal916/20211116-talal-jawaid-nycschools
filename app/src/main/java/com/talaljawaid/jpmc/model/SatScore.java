package com.talaljawaid.jpmc.model;

public class SatScore {
    private String dbn;
    private String readingScore;
    private String mathScore;
    private String writingScore;

    public SatScore(String dbn, String readingScore, String mathScore, String writingScore) {
        this.dbn = dbn;
        this.readingScore = readingScore;
        this.mathScore = mathScore;
        this.writingScore = writingScore;
    }

    public String getMathScore() {
        return mathScore;
    }

    public String getReadingScore() {
        return readingScore;
    }

    public String getWritingScore() {
        return writingScore;
    }
}
