package com.talaljawaid.jpmc;

import static org.junit.Assert.*;

import com.talaljawaid.jpmc.model.SatScore;
import com.talaljawaid.jpmc.model.School;

import org.junit.Test;

public class SchoolTest{

    @Test
    public void School_isCorrect() {
        String id = "1";
        String name = "Xavier's School for Gifted Youngsters";
        String address = "123 Main St, New York City, NY, 12345";
        String readingSatScore = "600";
        String writingSatScore = "500";
        String scienceSatScore = "450";
        String city = "New York City";
        String state_code = "NY";
        String overview = "We help gifted mutants";
        String latitude= "40.000";
        String longitude = "39.324";
        String phoneNumber = "1238675309";
        String zipCode = "12345";

        School school = new School(id, name, address, phoneNumber, zipCode, new SatScore(id, readingSatScore, scienceSatScore, writingSatScore), overview, longitude, latitude, city, state_code);
        assertEquals(school, school);
    }

    @Test
    public void School_isIncorrect() {
        String id = "1";
        String differentId = "2";
        String name = "Xavier's School for Gifted Youngsters";
        String address = "123 Main St, New York City, NY, 12345";
        String readingSatScore = "600";
        String writingSatScore = "500";
        String scienceSatScore = "450";
        String city = "New York City";
        String state_code = "NY";
        String overview = "We help gifted mutants";
        String latitude= "40.000";
        String longitude = "39.324";
        String phoneNumber = "1238675309";
        String zipCode = "12345";
        School school = new School(id, name, address, phoneNumber, zipCode, new SatScore(id, readingSatScore, scienceSatScore, writingSatScore), overview, longitude, latitude, city, state_code);
        School differentSchool = new School(differentId, name, address, phoneNumber, zipCode, new SatScore(differentId, readingSatScore, scienceSatScore, writingSatScore), overview, longitude, latitude, city, state_code);
        assertNotEquals(school, differentSchool);
    }
}